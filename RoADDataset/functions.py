import pickle as pkl
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from importlib import resources

def get(filename):
    return str( resources.files('RoADDataset').joinpath(filename) )

class Dataset():
    def __init__(self,normalize=True):
        with open(get('data/columns.pkl'),'rb') as f:
            self.columns=pkl.load(f)
        setNames=['training','collision','control','weight','velocity']
        self.sets={}
        for setName in setNames:
            with open(get(f'data/{setName}.pkl'),'rb') as f:
                self.sets[setName]=pkl.load(f)

        if normalize:
            self.normalizer=MinMaxScaler().fit(np.concatenate(self.sets['training'],axis=0))
            for setName in setNames:
                for recordingIx in range(len(self.sets[setName])):
                    normalized=self.normalizer.transform(self.sets[setName][recordingIx][:,:86])
                    label = self.sets[setName][recordingIx][:,86:]
                    self.sets[setName][recordingIx]=np.concatenate([normalized,label],axis=-1)
