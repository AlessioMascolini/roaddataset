import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='Road Dataset',
    version='1.0.0',
    author='Alessio Mascolini',
    author_email='alessio.mascolini@polito.it',
    description='RObotic Arm Dataset',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/AlessioMascolini/roaddataset/',
    license='MIT',
    packages=['RoADDataset'],
    install_requires=['scikit-learn','numpy'],
    include_package_data=True
)