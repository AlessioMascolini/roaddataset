# RoAD: Robotic Arm Dataset

This repository contains the code and dataset for the paper "Robotic Arm Dataset (RoAD): a Dataset to Support the Design and Test of Anomaly Detection in a Production Line". The dataset is designed to support the design, development, and testing of anomaly detection systems in the context of production lines, as well as other applications involving robotic arms.

## Installation

You can install this package using pip. Run the following command:

```
pip install git+https://gitlab.com/AlessioMascolini/roaddataset/
```

## Usage

Once installed, you can use the `Dataset` class from the `RoADDataset` package. This class contains five subsets namely: 'training', 'collision', 'control', 'weight', and 'velocity', as described in the paper. 

Here's a simple example on how to import and use the `Dataset` class:

```python
from RoADDataset import Dataset

# Instantiate the Dataset
dataset = Dataset()

# Access a subset
training_subset = dataset.sets['training']
```

training_subset will be a list of all recordings belonging to that subset

## Normalization

The `Dataset` class comes with a normalization feature which employs minmax normalization technique and is enabled by default. To use this feature, pass a truthy value to the `normalize` parameter when creating an instance of the `Dataset` class:

```python
# Instantiate the Dataset with normalization
normalized_dataset = Dataset(normalize=True)
```

In addition, an instance of the `sklearn.preprocessing.MinMaxScaler` class is accessible via the `normalizer` attribute of the `Dataset` class, which can be used for further transformations if required.

```python
# Access the sklearn normalizer
normalizer = normalized_dataset.normalizer
```

## Channels

The data feature 87 channels in the following order

1. Robot action ID
2. Apparent power
3. Current
4. Frequency
5. Phase angle
6. Power
7. Power factor
8. Reactive power
9. Voltage

### Joint 1
10. X-axis acceleration
11. Y-axis acceleration
12. Z-axis acceleration
13. X-axis angular velocity
14. Y-axis angular velocity
15. Z-axis angular velocity
16. Quaternion orientation component 1
17. Quaternion orientation component 2
18. Quaternion orientation component 3
19. Quaternion orientation component 4
20. Temperature

### Joint 2
21. X-axis acceleration
22. Y-axis acceleration
23. Z-axis acceleration
24. X-axis angular velocity
25. Y-axis angular velocity
26. Z-axis angular velocity
27. Quaternion orientation component 1
28. Quaternion orientation component 2
29. Quaternion orientation component 3
30. Quaternion orientation component 4
31. Temperature

### Joint 3
32. X-axis acceleration
33. Y-axis acceleration
34. Z-axis acceleration
35. X-axis angular velocity
36. Y-axis angular velocity
37. Z-axis angular velocity
38. Quaternion orientation component 1
39. Quaternion orientation component 2
40. Quaternion orientation component 3
41. Quaternion orientation component 4
42. Temperature

### Joint 4
43. X-axis acceleration
44. Y-axis acceleration
45. Z-axis acceleration
46. X-axis angular velocity
47. Y-axis angular velocity
48. Z-axis angular velocity
49. Quaternion orientation component 1
50. Quaternion orientation component 2
51. Quaternion orientation component 3
52. Quaternion orientation component 4
53. Temperature

### Joint 5
54. X-axis acceleration
55. Y-axis acceleration
56. Z-axis acceleration
57. X-axis angular velocity
58. Y-axis angular velocity
59. Z-axis angular velocity
60. Quaternion orientation component 1
61. Quaternion orientation component 2
62. Quaternion orientation component 3
63. Quaternion orientation component 4
64. Temperature

### Joint 6
65. X-axis acceleration
66. Y-axis acceleration
67. Z-axis acceleration
68. X-axis angular velocity
69. Y-axis angular velocity
70. Z-axis angular velocity
71. Quaternion orientation component 1
72. Quaternion orientation component 2
73. Quaternion orientation component 3
74. Quaternion orientation component 4
75. Temperature

### Joint 7
76. X-axis acceleration
77. Y-axis acceleration
78. Z-axis acceleration
79. X-axis angular velocity
80. Y-axis angular velocity
81. Z-axis angular velocity
82. Quaternion orientation component 1
83. Quaternion orientation component 2
84. Quaternion orientation component 3
85. Quaternion orientation component 4
86. Temperature

### Anomaly
87. Anomaly label

Channel 87 is not available in the training data

## Citation

If you find this dataset useful for your research, please cite our paper.

```
@inproceedings{inproceedings,
author = {Mascolini, Alessio and Gaiardelli, Sebastiano and Ponzio, Francesco and Dall'Ora, Nicola and Macii, Enrico and Vinco, Sara and Di Cataldo, Santa and Fummi, Franco},
year = {2023},
month = {10},
pages = {1-7},
title = {Robotic Arm Dataset (RoAD): A Dataset to Support the Design and Test of Machine Learning-Driven Anomaly Detection in a Production Line},
doi = {10.1109/IECON51785.2023.10311726}
}
```

